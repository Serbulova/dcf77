library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity clock_recovery is

    Port ( 
				clk: in std_logic;
				evenlope_input: in std_logic_vector(4 downto 0);
				clock_output: out  STD_LOGIC := '0'
			 );

end clock_recovery;

architecture Behavioral of clock_recovery is

	--constant const_counter_second_eqivalent: unsigned(24 downto 0) := to_unsigned(200000, 25); -- for 100 Hz uncomment only one
	constant const_counter_second_eqivalent: unsigned(24 downto 0) := to_unsigned(20000000, 25); -- for 1Hz uncomment only one
	--for 100 Hz = 200 000
	--for 1Hz = 20 000 000 
	
	constant const_signal_level_low: unsigned(24 downto 0) := const_counter_second_eqivalent / 2;
	
	signal counter: unsigned(24 downto 0) := to_unsigned(0, 25);
	signal counter2: unsigned(24 downto 0) := to_unsigned(0, 25);
	
	signal value: unsigned(24 downto 0) := to_unsigned(0, 25);
   
	signal clock_reg: STD_LOGIC := '0';
	
	-- clean edge detector
	constant first_nosignal_zone_counter_max: unsigned(24 downto 0) := resize(const_counter_second_eqivalent/10,25);
	constant last_nosignal_zone_counter_min: unsigned(24 downto 0) := const_counter_second_eqivalent - first_nosignal_zone_counter_max;
	
	constant nosignal_noise_treshold: unsigned(24 downto 0) := const_counter_second_eqivalent / 100000 ; --dat 100
	
	signal first_nosignal_zone_value: unsigned(24 downto 0) := to_unsigned(0, 25);
	signal last_nosignal_zone_value_zero: unsigned(24 downto 0) := to_unsigned(0, 25);
	signal last_nosignal_zone_value: unsigned(24 downto 0) := to_unsigned(0, 25);
	
	--garbage 
	
--	signal value: unsigned(26 downto 0) := to_unsigned(0, 27);
	
	
	
	signal value_low_mean: unsigned(26 downto 0) := to_unsigned(0, 27); --61600
	signal value_high_mean: unsigned(26 downto 0) := to_unsigned(0, 27);--85500
	signal value_treshold: unsigned(26 downto 0) := to_unsigned(0, 27);
	signal value_count: unsigned(6 downto 0) := to_unsigned(0, 7);
	
	-- clock recovery
	signal counter_middle: unsigned(24 downto 0) := resize(const_counter_second_eqivalent/2,25);
	signal value_low_mean_half: unsigned(26 downto 0) := to_unsigned(0, 27); 
	signal value_high_mean_half: unsigned(26 downto 0) := to_unsigned(0, 27);
	
	signal reg1: STD_LOGIC := '0';
	signal reg2: STD_LOGIC := '0';
	
	signal counter_low_error: signed(24 downto 0) := to_signed(0, 25);
	signal counter_high_error: signed(24 downto 0) := to_signed(0, 25);
	signal counter_high_error_mean: signed(24 downto 0) := to_signed(0, 25);
	-- end clock recovery

	--end
	
begin

process(clk)
	begin
		
		if clk'event and clk = '1' then
			
			--no signal zone
			if counter = first_nosignal_zone_counter_max then
				first_nosignal_zone_value <= value;
			end if;
			
			if counter = last_nosignal_zone_counter_min then
				last_nosignal_zone_value_zero <= value;
			end if;
			--end no signal zone
			
			if counter < first_nosignal_zone_counter_max and value > nosignal_noise_treshold then
				--reset counter and value
				value <= to_unsigned(0, 25);
				counter <= to_unsigned(0, 25);
			
			elsif ( counter >= const_counter_second_eqivalent ) then
			
				if ( clock_reg = '0' ) then
					clock_output <= '1';
					clock_reg <= '1';
				else
					clock_output <= '0';
					clock_reg <= '0';
				end if;
				
				counter <= to_unsigned(0, 25);
			
			else
			
				value <= value + resize( unsigned( not evenlope_input (3 downto 3) ), 25 );
				counter <= counter + 1;
			
			end if;
		
		end if;
	end process;

end Behavioral;