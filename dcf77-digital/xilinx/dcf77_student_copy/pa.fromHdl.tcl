
# PlanAhead Launch Script for Pre-Synthesis Floorplanning, created by Project Navigator

create_project -name dcf77_student_copy -dir "/users/students/r0692310/dcf77-digital/xilinx/dcf77_student_copy/planAhead_run_1" -part xc3s250etq144-4
set_param project.pinAheadLayout yes
set srcset [get_property srcset [current_run -impl]]
set_property target_constrs_file "dcf77.ucf" [current_fileset -constrset]
set hdlfile [add_files [list {uart_transmitter.vhd}]]
set_property file_type VHDL $hdlfile
set_property library work $hdlfile
set hdlfile [add_files [list {display_driver.vhd}]]
set_property file_type VHDL $hdlfile
set_property library work $hdlfile
set hdlfile [add_files [list {debouncer.vhd}]]
set_property file_type VHDL $hdlfile
set_property library work $hdlfile
set hdlfile [add_files [list {toplevel.vhd}]]
set_property file_type VHDL $hdlfile
set_property library work $hdlfile
set_property top toplevel $srcset
add_files [list {dcf77.ucf}] -fileset [get_property constrset [current_run]]
open_rtl_design -part xc3s250etq144-4
