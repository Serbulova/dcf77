library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity level_measurement is
    port ( 
				clk: in std_logic;
				evenlope_in: in std_logic_vector(4 downto 0);
				level_output: out std_logic_vector(6 downto 0):= std_logic_vector(to_unsigned(0, 7))
			 );
end level_measurement;

architecture Behavioral of level_measurement is
	
	--constant const_counter_second_eqivalent: unsigned(24 downto 0) := to_unsigned(200000, 25); -- for 100 Hz uncomment only one
	--constant const_counter_second_eqivalent: unsigned(24 downto 0) := to_unsigned(20000000, 25); -- for 1Hz uncomment only one
	
	--2 na 26 = 3,35s
	--2 na 27 = 6,7s
	constant size_of_window: unsigned(25 downto 0) := to_unsigned(2, 26) sll 24; -- pre 1hz clock
	--constant size_of_window: unsigned(25 downto 0) := resize(to_unsigned(2, 26) sll 18, 26); --pre 100hy clock
	
	
	signal counter: unsigned(28 downto 0) := to_unsigned(0, 29);
	signal value: unsigned(5 downto 0) := to_unsigned(0, 6);
	signal value_integrated: unsigned(32 downto 0) := to_unsigned(0, 33);
	
begin

process(clk)
	begin
		
		if clk'event and clk = '1' then
			
			if evenlope_in(4 downto 4) = "0" then 
				if evenlope_in(3 downto 3) = "0" then
					if evenlope_in(2 downto 2) = "0" then
						if evenlope_in(1 downto 1) = "0" then
							if evenlope_in(0 downto 0) = "0" then
								value <= to_unsigned(0, 6);
							else
								value <= to_unsigned(10, 6);
							end if;
						else 
							value <= to_unsigned(20, 6);
						end if;
					else 
						value <= to_unsigned(30, 6);
					end if;
				else 
					value <= to_unsigned(40, 6);
				end if;
			else 
				value <= to_unsigned(50, 6);
			end if;

			if ( counter < size_of_window ) then
				counter <= counter + 1;
				value_integrated <= value_integrated + value;
			else 
				counter <= to_unsigned(0, 29);
				
				level_output <= std_logic_vector(resize((value_integrated / size_of_window) * 2, 7));
				
				value_integrated <= to_unsigned(0, 33);
			end if;
							
		end if;
		
	end process;

end Behavioral;
