library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity gain_control is
    Port ( clk: in std_logic;
			  evenlope_in: in std_logic_vector(4 downto 0);
			  treshold_input: in  std_logic_vector(26 downto 0);
			  gain_output: out std_logic_vector(5 downto 0):= std_logic_vector(to_unsigned(0, 6));
			  saturation: in std_logic;
			  signal_level_output: out std_logic_vector(2 downto 0):= std_logic_vector(to_unsigned(0, 3));
			  lock: in std_logic;
			  clock: in std_logic	
			  );
end gain_control;

architecture Behavioral of gain_control is
	
	--constant const_counter_second_eqivalent: unsigned(24 downto 0) := to_unsigned(200000, 25); -- for 100 Hz uncomment only one
	constant const_counter_second_eqivalent: unsigned(24 downto 0) := to_unsigned(20000000, 25); -- for 1Hz uncomment only one
	
	constant const_counter_second_eqivalent_ms: unsigned(24 downto 0) := const_counter_second_eqivalent / 100; 
	
	constant const_signal_level_low: unsigned(24 downto 0) := const_counter_second_eqivalent / 10;
	constant const_signal_level_high: unsigned(24 downto 0) := resize ( const_signal_level_low * 3, 25);
	
	signal agc_value: unsigned(5 downto 0):= to_unsigned(0, 6);
	--signal agc_output: 
	signal value_treshold: unsigned(26 downto 0) := to_unsigned(0, 27);
	signal counter_small: unsigned(3 downto 0):= to_unsigned(0, 4);
	signal reg1: STD_LOGIC := '0';
	
	--pokus 2
	signal counter: unsigned(24 downto 0) := to_unsigned(0, 25);
	
	signal ev5_value: unsigned(24 downto 0) := to_unsigned(0, 25);
	signal ev4_value: unsigned(24 downto 0) := to_unsigned(0, 25);
	signal ev3_value: unsigned(24 downto 0) := to_unsigned(0, 25);
	signal ev2_value: unsigned(24 downto 0) := to_unsigned(0, 25);
	signal ev1_value: unsigned(24 downto 0) := to_unsigned(0, 25);
	
	signal init_fast_gain: STD_LOGIC := '1';
	
	signal level: unsigned(2 downto 0) := to_unsigned(0, 3);
	
begin

process(clk)
	begin
		
		if clk'event and clk = '1' then
			
			if ( init_fast_gain = '1' and evenlope_in (0 downto 0) = "0" and agc_value < 63 and counter > const_counter_second_eqivalent_ms ) then
			
				agc_value <= agc_value + 1;
				
				counter <= to_unsigned( 0, 25 );
				ev5_value <= to_unsigned( 0, 25 );
				ev4_value <= to_unsigned( 0, 25 );
				ev3_value <= to_unsigned( 0, 25 );
				ev2_value <= to_unsigned( 0, 25 );
				ev1_value <= to_unsigned( 0, 25 );
			
			else
			
				if ( counter >= const_counter_second_eqivalent ) then
				
					init_fast_gain <= '0';
					
					--level
					
					if ( ev5_value < const_signal_level_low/10 ) then
						if ( ev4_value < const_signal_level_low/10 ) then
							if ( ev3_value < const_signal_level_low/10 ) then
								if ( ev2_value < const_signal_level_low/10 ) then
									if ( ev1_value < const_signal_level_low/10 ) then
										level <= to_unsigned(0, 3);
									else
										level <= to_unsigned(1, 3);
									end if;
								else
									level <= to_unsigned(2, 3);
								end if;
							else
								level <= to_unsigned(3, 3);
							end if;
						else
							level <= to_unsigned(4, 3);
						end if;
					--elsif ( ev5_value < const_signal_level_low - const_signal_level_low/1 )
					else
						level <= to_unsigned(5, 3);
					end if;
					
					--
					
					--if ( ev5_value < const_signal_level_low/10 ) then
					--	level <= to_unsigned(4, 3);
					--elsif ( ev4_value < const_signal_level_low/10 ) then
					--	level <= to_unsigned(3, 3);
					--elsif ( ev3_value < const_signal_level_low/10 ) then
					--	level <= to_unsigned(2, 3);
					--elsif ( ev2_value < const_signal_level_low/10 ) then
					--	level <= to_unsigned(1, 3);
					--elsif ( ev1_value < const_signal_level_low/10 ) then
					--	level <= to_unsigned(0, 3);
					--end if;
					
					--if ( ev5_value > const_signal_level_low ) then
					--level <= to_unsigned(4, 3);
					--elsif ( ev4_value > const_signal_level_low and ev4_value > const_signal_level_low )
					
					--end level
					
					--simple addition
					
					if ( ev5_value < const_counter_second_eqivalent/10 and agc_value < 63 ) then
					
						agc_value <= agc_value + 1;
					
						counter <= to_unsigned( 0, 25 );
						ev5_value <= to_unsigned( 0, 25 );
						ev4_value <= to_unsigned( 0, 25 );
						ev3_value <= to_unsigned( 0, 25 );
						ev2_value <= to_unsigned( 0, 25 );
						ev1_value <= to_unsigned( 0, 25 );
					
					--end if;
					
					--end simple addition
					
					--simple gain control
					--elsif ( ev1_value < const_counter_second_eqivalent - const_counter_second_eqivalent/10 and agc_value < 63 ) then
					--
					--	agc_value <= agc_value + 1;
					--
					--	counter <= to_unsigned( 0, 25 );
					--	ev5_value <= to_unsigned( 0, 25 );
					--	ev4_value <= to_unsigned( 0, 25 );
					--	ev3_value <= to_unsigned( 0, 25 );
					--	ev2_value <= to_unsigned( 0, 25 );
					--	ev1_value <= to_unsigned( 0, 25 );
					
					--end if;
					
					elsif ( ev5_value > const_counter_second_eqivalent/10 and agc_value > 0 ) then
					
						agc_value <= agc_value - 1;
					
						counter <= to_unsigned( 0, 25 );
						ev5_value <= to_unsigned( 0, 25 );
						ev4_value <= to_unsigned( 0, 25 );
						ev3_value <= to_unsigned( 0, 25 );
						ev2_value <= to_unsigned( 0, 25 );
						ev1_value <= to_unsigned( 0, 25 );
					
					--end if;
					
					else 
					
						counter <= to_unsigned( 0, 25 );
						ev5_value <= to_unsigned( 0, 25 );
						ev4_value <= to_unsigned( 0, 25 );
						ev3_value <= to_unsigned( 0, 25 );
						ev2_value <= to_unsigned( 0, 25 );
						ev1_value <= to_unsigned( 0, 25 );
					
					end if;
				
				else
				--end simple gain control
				
					counter <= counter + 1;
			
					ev5_value <= ev5_value + resize( unsigned( evenlope_in (4 downto 4) ), 25 );
					ev4_value <= ev4_value + resize( unsigned( evenlope_in (3 downto 3) ), 25 );
					ev3_value <= ev3_value + resize( unsigned( evenlope_in (2 downto 2) ), 25 );
					ev2_value <= ev2_value + resize( unsigned( evenlope_in (1 downto 1) ), 25 );
					ev1_value <= ev1_value + resize( unsigned( evenlope_in (0 downto 0) ), 25 );
				
				end if;
			
				--if not clock = reg1 then
				--	if counter_small < 15 then
				--		counter_small <= counter_small + 1;
				--	end if;
				--	reg1 <= clock;
				--	
				--	if (lock = '0' and counter_small > 5) or saturation = '1' then		--if (lock = '0' or saturation = '1') and counter > 5) then				
				--		if unsigned(treshold_input(26 downto 0)) > const_counter_second_eqivalent*2 or saturation = '1' then
				--			agc_value <= agc_value - 1;
				--			counter <= to_unsigned(0, 4); 
				--		end if;
				--		if unsigned(treshold_input(26 downto 0)) < const_counter_second_eqivalent/2 then
				--			agc_value <= agc_value + 1;
				--			counter <= to_unsigned(0, 4);
				--		end if;
				--	end if;
				--end if;
			
			end if;

			signal_level_output <= std_logic_vector(level);
			gain_output <= std_logic_vector(agc_value);
			
		end if;
		
	end process;

end Behavioral;
