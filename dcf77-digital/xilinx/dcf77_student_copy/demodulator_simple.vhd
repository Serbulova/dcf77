library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity demodulator_simple is
    Port ( clk: in std_logic;
			  input: in std_logic_vector(4 downto 0);
			  output: out  STD_LOGIC := '0';
			  output_treshold: out std_logic_vector(26 downto 0) := (others => '0');
			  output_lastvalue: out std_logic_vector(26 downto 0) := (others => '0');
			  output_clock: inout std_logic := '0';
			  agc_lock: out STD_LOGIC := '1';
			  agc_saturation: out STD_LOGIC := '0';
			  agc_value: in std_logic_vector(5 downto 0));
end demodulator_simple;

architecture Behavioral of demodulator_simple is

	signal inputnot: std_logic_vector(4 downto 0);
	signal counter: unsigned(24 downto 0) := to_unsigned(0, 25);
   signal value: unsigned(26 downto 0) := to_unsigned(0, 27);
	
	--constant const_counter_second_eqivalent: unsigned(24 downto 0) := to_unsigned(200000, 25); -- for 100 Hz uncomment only one
	constant const_counter_second_eqivalent: unsigned(24 downto 0) := to_unsigned(20000000, 25); -- for 1Hz uncomment only one
	--for 100 Hz = 200 000
	--for 1Hz = 20 000 000 
	
	signal value_low_mean: unsigned(26 downto 0) := to_unsigned(0, 27); --61600
	signal value_high_mean: unsigned(26 downto 0) := to_unsigned(0, 27);--85500
	signal value_treshold: unsigned(26 downto 0) := to_unsigned(0, 27);
	signal value_count: unsigned(6 downto 0) := to_unsigned(0, 7);
	
	-- clock recovery
	signal counter_middle: unsigned(24 downto 0) := resize(const_counter_second_eqivalent/2,25);
	signal value_low_mean_half: unsigned(26 downto 0) := to_unsigned(0, 27); 
	signal value_high_mean_half: unsigned(26 downto 0) := to_unsigned(0, 27);
	
	signal reg1: STD_LOGIC := '0';
	signal reg2: STD_LOGIC := '0';
	
	signal counter_low_error: signed(24 downto 0) := to_signed(0, 25);
	signal counter_high_error: signed(24 downto 0) := to_signed(0, 25);
	signal counter_high_error_mean: signed(24 downto 0) := to_signed(0, 25);
	-- end clock recovery
	
	--gain control
	signal agc_value_old: unsigned(5 downto 0) := to_unsigned(15, 6); 
	signal agc_request_gain_change: STD_LOGIC := '0';
	signal agc_direction: STD_LOGIC := '0'; --0 down 1 up
	signal agc_counter: unsigned(24 downto 0) := to_unsigned(0, 25);
	signal saturation: STD_LOGIC := '0';
	
	-- clean edge detector
	constant first_nosignal_zone_counter_max: unsigned(24 downto 0) := resize(const_counter_second_eqivalent/10,25);
	constant last_nosignal_zone_counter_min: unsigned(24 downto 0) := const_counter_second_eqivalent - first_nosignal_zone_counter_max;

	constant nosignal_noise_treshold: unsigned(26 downto 0) := to_unsigned(0, 27); --dat 100
	
	signal first_nosignal_zone_value: unsigned(26 downto 0) := to_unsigned(0, 27);
	signal last_nosignal_zone_value_zero: unsigned(26 downto 0) := to_unsigned(0, 27);
	signal last_nosignal_zone_value: unsigned(26 downto 0) := to_unsigned(0, 27);
	--end
	
begin

process(clk)
	begin
		inputnot <= not input;
		
		if clk'event and clk = '1' then
		
			if agc_value_old /= unsigned(agc_value) then
				agc_value_old <= unsigned(agc_value);
				agc_lock <= '1';
				agc_counter <= to_unsigned(0, 25);
				value_count <= to_unsigned(0, 7);
				value <= to_unsigned(0, 27);
				counter <= to_unsigned(0, 25);
				agc_saturation <= '0';
				saturation <= '0';
			end if;
		
			if value_count = 0 and saturation = '0' then
				agc_lock <= '1';
			end if;
		
			counter <= counter + 1;
			--value <= value + (resize(unsigned(inputnot(4 downto 3)), 27) sll 0) + (resize(unsigned(inputnot(3 downto 2)), 27) sll 0) + (resize(unsigned(inputnot(2 downto 1)), 27) sll 0) + (resize(unsigned(inputnot(1 downto 0)), 27) sll 0) + unsigned(inputnot(0 downto 0));
			value <= value + (resize(unsigned(inputnot(4 downto 3)), 27) sll 0) + (resize(unsigned(inputnot(3 downto 2)), 27) sll 0) + (resize(unsigned(inputnot(2 downto 1)), 27) sll 0) + (resize(unsigned(inputnot(1 downto 0)), 27) sll 0) + unsigned(inputnot(0 downto 0));
			
			
			if value_count = 0 and counter < first_nosignal_zone_counter_max and value > nosignal_noise_treshold then
				--reset counter and value
				agc_counter <= agc_counter + counter;
				value_count <= to_unsigned(0, 7);
				value <= to_unsigned(0, 27);
				counter <= to_unsigned(0, 25);
				if(agc_counter > const_counter_second_eqivalent + const_counter_second_eqivalent/32) then
					agc_lock <= '0';
					agc_saturation <= '1';
					saturation <= '1';
				end if;
			end if;
			
			if value_count > 5 then
				agc_lock <= '0';
			end if;
			
			--no signal zone
			if counter = first_nosignal_zone_counter_max then
				first_nosignal_zone_value <= value;
			end if;
			
			if counter = last_nosignal_zone_counter_min then
				last_nosignal_zone_value_zero <= value;
			end if;
			--end no signal zone
			
			-- clock recovery
			if counter = counter_middle then
				output_clock <= not output_clock;
			end if;
			
			if  value_count > 5 AND reg1 = '0' ANd value >= value_low_mean_half then 
				counter_low_error <= to_signed(to_integer(counter),25) - to_signed(to_integer(counter_middle), 25);
				reg1 <= '1';
			end if;
			if  value_count > 5 AND reg2 = '0' AND value >= value_high_mean_half then 
				counter_high_error <= to_signed(to_integer(counter),25) - to_signed(to_integer(counter_middle), 25);
				reg2 <= '1';
			end if;
			-- end clock recovery
			
				--end of 1s timer
			if counter >= const_counter_second_eqivalent then --for 100Hz
				
				-- clock recovery
				reg1 <= '0';
				reg2 <= '0';
				-- end clock recovery
				
				--no signal zone
				last_nosignal_zone_value <= value - last_nosignal_zone_value_zero;
				--end no signal zone
			
				--compute
				value_count <= value_count + 1;
				
				if value_count = to_unsigned(0, 7) then
					value_high_mean <= value;
					value_low_mean <= value;
				end if;
				
				value_treshold <= resize((value_high_mean - value_low_mean)/2,27) + value_low_mean;
				
				if value > value_treshold then
					if value_count = 1 and value > value_high_mean and (value - value_high_mean) > value_high_mean then
						value_high_mean <= value;
					end if;
					if value_count > 1 and value_count < 5 then
						value_high_mean <= resize((value_high_mean + value)/2,27);
					end if;
					if value_count >= 5 then
						--value_high_mean <= resize((value_high_mean * value_count + value)/(value_count + 1),27);
						value_high_mean <= resize((value_high_mean * 3 + value)/(3 + 1),27);
					end if;
					
					output <= '1';
					
					-- clock recovery
					value_high_mean_half <= value_high_mean srl 1;
					
					if value_count > 5 then
					--counter_high_error_mean <= resize((counter_high_error_mean * (to_signed(to_integer(value_count),7) + to_signed(-5,7)) + counter_high_error)/(to_signed(to_integer(value_count),7) + to_signed(-5,7) + to_signed(1,7)),25);
					counter_high_error_mean <= resize(((counter_high_error_mean * to_signed(3,7)) + counter_high_error)/(to_signed(4,7)),25);
					--counter_high_error_mean <= to_signed(0,27); --toto dat dopice
					-- end clock recovery
					end if;
							
				else
					if value_count = 1 and value < value_low_mean and (value_low_mean - value) > value then
						value_low_mean <= value;
					end if;
					if value_count > 1 and value_count < 5 then
						value_low_mean <= resize((value_low_mean + value)/2,27);
					end if;
					if value_count >= 5 then 
						--value_low_mean <= resize((value_low_mean * value_count + value)/(value_count + 1),27);
						value_low_mean <= resize((value_low_mean * 3 + value)/(3 + 1),27);
					end if;
					
					output <= '0';
					-- clock recovery
					value_low_mean_half <= value_low_mean srl 1;
					-- end clock recovery
				end if;
				
				output_lastvalue <= std_logic_vector(value);
				
				--reset counter and value
				value <= to_unsigned(0, 27);
				counter <= to_unsigned(0, 25);
				
			end if;
			
			output_treshold <= std_logic_vector(value_treshold);
			
		end if;
	end process;

end Behavioral;
