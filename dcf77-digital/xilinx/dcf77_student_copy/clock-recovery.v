`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   17:02:13 03/07/2018
// Design Name:   clock_recovery
// Module Name:   /users/students/r0692310/dcf77-digital/xilinx/dcf77_student_copy/clock-recovery.v
// Project Name:  dcf77_student_copy
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: clock_recovery
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module clock-recovery;

	// Inputs
	reg i1;
	reg i2;
	reg i3;
	reg i4;
	reg i5;

	// Outputs
	wire o1;
	wire o2;
	wire o3;
	wire o4;
	wire o5;

	// Instantiate the Unit Under Test (UUT)
	clock_recovery uut (
		.i1(i1), 
		.i2(i2), 
		.i3(i3), 
		.i4(i4), 
		.i5(i5), 
		.o1(o1), 
		.o2(o2), 
		.o3(o3), 
		.o4(o4), 
		.o5(o5)
	);

	initial begin
		// Initialize Inputs
		i1 = 0;
		i2 = 0;
		i3 = 0;
		i4 = 0;
		i5 = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here

	end
      
endmodule

