library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity testbench1 is
end testbench1;

architecture bhv of testbench1 is
	
	signal clk: std_logic := '0';
	signal analog_comp: std_logic_vector(5 downto 0) := (others => '0');
	signal evenlope: std_logic_vector(4 downto 0) := (others => '0');
	signal output_demodulator: std_logic := '0';
	signal treshold: std_logic_vector(26 downto 0);
	signal clock: std_logic := '0';
	signal agc_lock: std_logic := '1';
	signal agc_saturation: std_logic := '0';
	signal agc_gain: std_logic_vector(5 downto 0) := (others => '0');
	signal level_measurement: std_logic_vector(6 downto 0):= std_logic_vector(to_unsigned(0, 7));
	signal dcf: std_logic;
	signal value0: unsigned(5 downto 0);
	signal value1: unsigned(5 downto 0);
   signal value2: unsigned(5 downto 0);
	signal value3: unsigned(5 downto 0);
	
	
	constant clk_period: time := 50 ns;
	
	-- csv reader definition
	component csvreader is
		generic(
			clk_period: integer;
			csvfile: string
		); 
		port(
			data: out std_logic
		);
	end component;
	
begin
	
	-- comparator input -- na fku ficime normalne
	csvreader_comp0: entity work.csvreader generic map(clk_period => 50, csvfile => "wave7f_comp0.csv") port map(data => analog_comp(0));
	csvreader_comp1: entity work.csvreader generic map(clk_period => 50, csvfile => "wave7f_comp1.csv") port map(data => analog_comp(1));
	csvreader_comp2: entity work.csvreader generic map(clk_period => 50, csvfile => "wave7f_comp2.csv") port map(data => analog_comp(2));
	csvreader_comp3: entity work.csvreader generic map(clk_period => 50, csvfile => "wave7f_comp3.csv") port map(data => analog_comp(3));
	csvreader_comp4: entity work.csvreader generic map(clk_period => 50, csvfile => "wave7f_comp4.csv") port map(data => analog_comp(4));
	csvreader_comp5: entity work.csvreader generic map(clk_period => 50, csvfile => "wave7f_comp5.csv") port map(data => analog_comp(5));
	
	evenlope_detector5: entity work.evenlope_detector port map( clk => clk, input => analog_comp(5), output => evenlope(4) );
	evenlope_detector4: entity work.evenlope_detector port map( clk => clk, input => analog_comp(4), output => evenlope(3) );
	evenlope_detector3: entity work.evenlope_detector port map( clk => clk, input => analog_comp(3), output => evenlope(2) );
	evenlope_detector2: entity work.evenlope_detector port map( clk => clk, input => analog_comp(2), output => evenlope(1) );
	evenlope_detector1: entity work.evenlope_detector port map( clk => clk, input => analog_comp(1), output => evenlope(0) );
	
	--demodulator
	demodulator1: entity work.demodulator port map(
		clk => clk,
		input => evenlope,
		output => output_demodulator,
		output_treshold => treshold,
	--	output_clock => clock,
		agc_lock => agc_lock,
	   agc_saturation => agc_saturation,
	   agc_value => agc_gain
	);
	
	--clock recovery
	clock_recovery: entity work.clock_recovery port map(
		clk => clk,
		evenlope_input => evenlope,
		clock_output => clock
	);
	
	--gain control
	gain_control: entity work.gain_control port map(
		clk => clk,
		treshold_input => treshold,
		gain_output => agc_gain,
		saturation => agc_saturation,
		lock => agc_lock,
		clock => clock,
		evenlope_in => evenlope
	);
	
	--level_measurement
	level_measurement1: entity work.level_measurement port map(
		clk => clk,
		level_output => level_measurement,
		evenlope_in => evenlope
	);
	
	--decoder
	decoder_indicator: entity work.decoder_indicator port map(
	   clk => clk,
		dcf => output_demodulator,
		symbol0 => value0,
		symbol1 => value1,
		symbol2 => value2,
		symbol3 => value3
	);
		
	-- clock process
	process
	begin
		clk <= '0';
		wait for clk_period;
		while true loop
			clk <= '1';
			wait for clk_period/2;
			clk <= '0';
			wait for clk_period/2;
		end loop;
	end process;
	
end;
