library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
 
entity decoder_indicator is
    port (
        clk:        in std_logic;   -- clock
        dcf:        in std_logic;   -- dcf bit from demodulator
        sec0:       out std_logic;  -- second 0 strobe     
        symbol0:    out unsigned(5 downto 0);
        symbol1:    out unsigned(5 downto 0);
        symbol2:    out unsigned(5 downto 0);
        symbol3:    out unsigned(5 downto 0)
        );  
end decoder_indicator;
 
architecture bhv of decoder_indicator is
    constant seclen: integer := 55000000;    -- clock per 1.1 sec
    constant bitlen: integer := 7500000;     -- clock per 150ms bit threshold
    signal frontsr: std_logic_vector ( 2 downto 0 ); -- shift register for dcf front
    signal bitsr: std_logic_vector ( 15 downto 0 ) := ( others => '0' );  -- shift register for dcf time data
    signal hdigits: std_logic_vector ( 11 downto 0 ) := ( others => '0' );
    signal mdigits: std_logic_vector ( 11 downto 0 ) := ( others => '0' );
    signal tbyte: std_logic_vector ( 7 downto 0 ) := ( others => '0' );
    signal clockcnt: integer range 0 to seclen := 0; -- clock counter
    signal bitcnt: integer range 0 to 58 := 0; -- bit counter
     
    procedure byte_to_bcd (  
        byte: in std_logic_vector(7 downto 0);
        signal bcd: inout std_logic_vector(11 downto 0) )
    is
        variable i : integer:=0;
        variable bint : std_logic_vector(7 downto 0) := byte;
    begin
        bcd <= (others => '0');
        for i in 0 to 7 loop
            bcd(11 downto 1) <= bcd(10 downto 0);
            bcd(0) <= bint(7);
            bint(7 downto 1) := bint(6 downto 0);
            bint(0) :='0';  
            if(i < 7 and bcd(3 downto 0) > "0100") then
                bcd(3 downto 0) <= bcd(3 downto 0) AND "0011";
            end if;
            if(i < 7 and bcd(7 downto 4) > "0100") then
                bcd(7 downto 4) <= bcd(7 downto 4) AND "0011";
            end if;
            if(i < 7 and bcd(11 downto 8) > "0100") then
                bcd(11 downto 8) <= bcd(11 downto 8) AND "0011";
            end if;
        end loop;
    end procedure byte_to_bcd;
      
    begin
process(clk)
     begin
        
        if clk'event and clk = '1' then
            frontsr <= frontsr(1 downto 0) & dcf;
            if(clockcnt < seclen) then
                clockcnt <= clockcnt + 1;
            end if;
             
            sec0 <= '0';   
             
            if(frontsr(2 downto 1) = "01") then     -- dcf bit raise
                if (clockcnt = seclen) then              -- 59s sec
                     
                    -- process time data from bitsr
                    if(bitsr(0)= '1') then  
                        if((bitsr(8) xor bitsr(7) xor bitsr(6) xor bitsr(5) xor bitsr(4) xor bitsr(3) xor bitsr(2) xor bitsr(1)) = '0') then
                            if ((bitsr(15) xor bitsr(14) xor bitsr(13) xor bitsr(12) xor bitsr(11) xor bitsr(10) xor bitsr(9)) = '0') then
                                sec0 <= '1';
                                tbyte <= '0' & bitsr( 7 downto 1);
                                byte_to_bcd(tbyte, mdigits);
                                tbyte <= "00" & bitsr( 14 downto 9);
                                byte_to_bcd(tbyte, hdigits);
                                symbol0 <= unsigned("00" & hdigits(7  downto 4));
                                symbol1 <= unsigned("00" & hdigits(11 downto 8));
                                symbol2 <= unsigned("00" & mdigits(7  downto 4));
                                symbol3 <= unsigned("00" & mdigits(11 downto 8));
                            end if;
                        end if;
                    end if;
                     
                    bitsr <= (others=>'0');
                    bitcnt <= 0;
                end if;
                clockcnt <= 0;
            end if;
              
            if(frontsr(2 downto 1) = "10") then     -- dcf bit fall
                if(bitcnt > 19 and bitcnt < 36) then
                    if(clockcnt < bitlen) then           -- bit 0
                        bitsr <= '0' & bitsr(15 downto 1);
                    else                            -- bit 1
                        bitsr <= '1' & bitsr(15 downto 1);
                    end if;
                end if;     
                bitcnt <= bitcnt + 1;
            end if;
             
        end if;
    end process;
end bhv;
