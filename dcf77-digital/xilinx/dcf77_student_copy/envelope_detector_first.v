timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   16:24:13 03/07/2018
// Design Name:   envelope_detector
// Module Name:   /users/students/r0692310/dcf77-digital/xilinx/dcf77_student_copy/envelope_detector_first.v
// Project Name:  dcf77_student_copy
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: envelope_detector
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module envelope_detector_first;

	// Inputs
	reg i1;

	// Outputs
	wire o1;

	// Instantiate the Unit Under Test (UUT)
	envelope_detector uut (
		.i1(i1), 
		.o1(o1)
	);

	initial begin
		// Initialize Inputs
		i1 = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here

	end
      
endmodule

