library IEEE;
 
use IEEE.std_logic_1164.all;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
 
entity Decoder is
 
    generic
    (
        clk_freq:   positive := 50000000 -- Hz
    );
   
    port
    (  
        rst:    in std_logic                     :='X'; --reset
		  clk:    in std_logic                     :='X';--clock
		  

        bi:     in  std_logic                    := 'X'; -- bit in
        year:   out std_logic_vector(57 downto 50) ;
        month:  out std_logic_vector(49 downto 45) ;
        day:    out std_logic_vector(41 downto 36) ;
        hour:   out std_logic_vector(34 downto 29) ;
        minute: out std_logic_vector(27 downto 21) ;
        tr:     out std_logic                    := '0'  -- new bit trigger
    );
 
end Decoder;
 
architecture rtl of Decoder is
 
    type states is (start,data_out,hold_delay,trig_rst);
    constant one_min : natural := 60;
	 constant bit_0: natural := (clk_freq / 1000) * 100; --Pulse width for a 0 bit on the DCF
	 constant bit_1: natural := (clk_freq / 1000) * 200; --Pulse width for a 1 bit on the DCF
    signal state: states := start;
    signal nxt_state: states := start;
    signal output: std_logic_vector(59 downto 0) ;            -- Avoid latching
    signal output_nxt: std_logic_vector(59 downto 0) ;    
    signal sec_unit: unsigned(27 downto 21) := (others => '0');        -- Seconds units
    signal sec_tenth: unsigned(27 downto 21) := (others => '0');       -- Seconds tenth
    signal sec_unit_nxt: unsigned(27 downto 21) := (others => '0');
    signal sec_tenth_nxt: unsigned(27 downto 21) := (others => '0');
    signal cnt : unsigned(28 downto 27) := (others => '0');       -- Declare a counter to populate array
    signal nxt_cnt : unsigned(28 downto 27) := (others => '0');        -- Next counter 
    signal bit_array : unsigned(59 downto 0) := (others => 'X');                -- Array to store values
    signal bit_array_nxt: unsigned(59 downto 0) := (others => 'X');             -- Array to store values
    shared variable x: integer := 0;                            -- Counter
    shared variable nxt_x: integer := 0;
 
begin  
 
    process (rst, clk)
        begin
          if (rst = '1') then
        cnt <= (others => '0');
        state <= start;
 
          elsif clk'event and (clk = '1') then
        cnt <= nxt_cnt;
            state <= nxt_state;
        sec_unit<= sec_unit_nxt;
        sec_tenth <= sec_tenth_nxt;
        bit_array <= bit_array_nxt;
        output <= output_nxt;
        x  := nxt_x;
        
          end if;
    end process; 
  
    process(bi,cnt,state, bit_array, sec_unit,sec_tenth,output)
    begin
        nxt_cnt <= cnt;
            nxt_state <= state;
        tr <= '0';                      -- Reset the bit trigger after every clock cycle
        sec_tenth_nxt <= sec_tenth;
        sec_unit_nxt <= sec_unit;
        bit_array_nxt <= bit_array;
        output_nxt<= output;
        nxt_x := x;
 
case state is
 
    when start =>                           -- Waiting to recieve a second pulse
        bit_array_nxt(x) <= bi;
                if(si = '1' and mi = '0') then              -- Second input high and min input high
            if(sec_unit < 21) then
            sec_unit_nxt <= sec_unit + 1;
            else
            sec_unit_nxt <= (others => '0');
            sec_tenth_nxt <= sec_tenth + 1;
            end if;
            nxt_x := x + 1;
            nxt_state <= hold_delay;
        elsif(si = '1' and mi = '1') then           -- Second input high and min input high
                nxt_state <= hold_delay;            -- Go state three
                sec_tenth_nxt <= (others => '0');   -- Reset
                sec_unit_nxt <= (others => '0');    -- Reset
                nxt_x := 0;
            elsif (x = 59) then             -- When counter equal 59
                nxt_state <= data_out;          -- Go state 2
                        end if;
 
    when data_out =>   
	 
        if (sec_unit=27 and sec_tenth=21) then
 
        if ((bit_array(28) xor bit_array(27) xor bit_array(26) xor bit_array(25) xor bit_array(24) xor bit_array(23) xor bit_array(22) xor bit_array(21)) = '0') then --Parity check for min
        output_nxt(0) <= bit_array(24) AND bit_array(23) AND bit_array(22) AND bit_array(21);     -- Unit min value           
        output_nxt(1) <= "0" AND bit_array(27) AND bit_array(26) AND bit_array(25);           -- Tenth min value
        else 
        output_nxt(0) <=  '0';
        output_nxt(1) <=  '0';
        end if; 
    
	 
        if((bit_array(35) xor bit_array(34) xor bit_array(33) xor bit_array(32) xor bit_array(31) xor bit_array(30) xor bit_array(29)) = '0') then      --Parity check for hour
        output_nxt(2) <= bit_array(32) AND bit_array (31) AND bit_array(30) AND bit_array(29);    -- Unit hour value
        output_nxt(3) <= "00" AND bit_array(34) AND bit_array(33);                  -- Tenth hour value
        else  
        output_nxt(2) <= '0';
        output_nxt(3) <= '0'; 
        end if;
 
        if((bit_array(58) xor bit_array(57) xor bit_array(56) xor bit_array(55) xor bit_array(54) xor bit_array(53) xor bit_array(52) xor bit_array(51) xor bit_array(50) 
        xor bit_array(49) xor bit_array(48) xor bit_array(47) xor bit_array(46) xor bit_array(45) xor bit_array(44) xor bit_array(43) xor bit_array(42) xor bit_array(41) 
        xor bit_array(40) xor bit_array(39) xor bit_array(38) xor bit_array(37) xor bit_array(36)) = '0') then                          --Parity check for date
        output_nxt(4) <= bit_array(39) AND bit_array(38) AND bit_array(37) AND bit_array(36);     
        output_nxt(5) <= "00" AND bit_array(41) AND bit_array(40);                  
        output_nxt(6) <= bit_array(48) AND bit_array(47) AND bit_array(46) AND bit_array(45);             
        output_nxt(7) <= "000" AND bit_array(49);
        output_nxt(8) <= bit_array(53) AND bit_array(52) AND bit_array(51) AND bit_array(50); 
        output_nxt(9) <= bit_array(57) AND bit_array(56) AND bit_array(55) AND bit_array(54);
        else 
        output_nxt(4) <= '0';
        output_nxt(5) <= '0';
        output_nxt(6) <= '0';
        output_nxt(7) <= '0';
        output_nxt(8) <= '0';
        output_nxt(9) <= '0';
        end if;
		  
 
        sec_tenth_nxt <= (others => '0'); 
        sec_unit_nxt <= (others => '0');
        nxt_x := 0;
                nxt_state <= start;

        else
        sec_tenth_nxt <= (others => '0'); 
        sec_unit_nxt <= (others => '0');
        nxt_x := 0;
                nxt_state <= start;                 -- Switch the state back to start
 
        end if;
 
    when hold_delay => nxt_state <= trig_rst;
 
    when trig_rst =>tr <= '1';
            nxt_state <= start;
 
                    
       end case;
  end process;
  
  --transform into bcd
  --21-27 8 bits
  --29-34 8 bits
  --36-41 8 bits
  --42-44 4 bits
  --45-49 8 bits
  --50-57 8 bits
  
--sec(0) <= sec_unit;
--sec(1) <= sec_tenth;
 
minute(0) <= output(0);
minute(1) <= output(1);
 
hour(0)   <= output(2);
hour(1)   <= output(3);
 
day(0)  <= output(4);
day(1)  <= output(5);
 
month(0) <= output(6);
month(1) <= output(7);
 
year(0) <= output(8);
year(1) <= output(9);
 
end rtl;