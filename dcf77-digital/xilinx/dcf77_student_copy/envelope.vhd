library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity evenlope_detector is
	port(
		clk: in std_logic;
		input: in std_logic;
		output: out std_logic
	);
end evenlope_detector;

architecture bhv of evenlope_detector is
	
	signal counter: unsigned(7 downto 0) := to_unsigned(0, 8);
	signal reg: std_logic := '0';
	
begin
	
	output <= reg;
	
	process(clk)
	begin
		if clk'event and clk = '1' then
		counter <= counter + 1;
		
		if input = '1' then
		reg <= '1';
		counter <= to_unsigned(0, 8);
		output <= reg;
		end if;
		
		if counter = (7 downto 0 => '1') then
		reg <= '0';
		counter <= to_unsigned(0, 8);
		end if;
		
		output <= reg;
		end if;
	end process;
	
end bhv;