library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity toplevel is
	port(
		clk: in std_logic;
		test_switches: in std_logic_vector(3 downto 0);
		test_buttons: in std_logic_vector(3 downto 0);
		test_leds: out std_logic_vector(7 downto 0);
		uart_rx: in std_logic;
		uart_tx: out std_logic;
		uart_rts_bar: out std_logic;
		uart_cts_bar: in std_logic;
		display_digits: out std_logic_vector(3 downto 0);
		display_segments: out std_logic_vector(6 downto 0);
		display_colon: out std_logic;
		analog_comp: in std_logic_vector(5 downto 0);
		analog_gain: out std_logic_vector(5 downto 0)
	);
end toplevel;

architecture bhv of toplevel is
	
	-- delay elements to avoid metastability
	signal test_switches_d: std_logic_vector(3 downto 0) := "0000";
	signal test_switches_dd: std_logic_vector(3 downto 0) := "0000";
	signal test_buttons_d: std_logic_vector(3 downto 0) := "0000";
	signal test_buttons_dd: std_logic_vector(3 downto 0) := "0000";
	signal analog_comp_d: std_logic_vector(5 downto 0) := "000000";
	signal analog_comp_dd: std_logic_vector(5 downto 0) := "000000";
	
	-- debounced signals
	signal test_switches_deb: std_logic_vector(3 downto 0);
	signal test_buttons_deb: std_logic_vector(3 downto 0);
	
	-- UART transmitter
	signal uart_transmitter_data: std_logic_vector(7 downto 0) := "00000000";
	signal uart_transmitter_push: std_logic := '0';
	
	-- display values
	signal display_value0: unsigned(5 downto 0);
	signal display_value1: unsigned(5 downto 0);
	signal display_value2: unsigned(5 downto 0);
	signal display_value3: unsigned(5 downto 0);
	
	-- binary to hexadecimal lookup table
	type hex_table_t is array(0 to 15) of std_logic_vector(7 downto 0);
	constant hex_table: hex_table_t := (
		X"30", X"31", X"32", X"33", X"34", X"35", X"36", X"37",
		X"38", X"39", X"61", X"62", X"63", X"64", X"65", X"66"
	);
	
	-- counter
	signal counter1: unsigned(20 downto 0) := to_unsigned(0, 21);
	signal counter2: unsigned(3 downto 0) := to_unsigned(0, 4);
	
	--my signals
	--signal analog_comp: std_logic_vector(5 downto 0) := (others => '0');
	signal evenlope: std_logic_vector(4 downto 0) := (others => '0');
	signal output_demodulator: std_logic := '0';
	signal treshold: std_logic_vector(26 downto 0);
	signal clock: std_logic := '0';
	signal clock_reg: std_logic := '0';
	signal agc_lock: std_logic := '1';
	signal agc_saturation: std_logic := '0';
	signal agc_gain: std_logic_vector(5 downto 0) := (others => '0');
	
	signal manual_gain: std_logic := '0';
	signal manual_gain_button_state: std_logic := '0';
	signal agc_gain_manual: std_logic_vector(5 downto 0) := (others => '0');
	
	signal manual_gain_bt0prev: std_logic := '0';
	signal manual_gain_bt1prev: std_logic := '0';
	
	signal gain_placeholder: std_logic_vector(5 downto 0) := (others => '0');
	signal clock_prev: std_logic := '0';
	--signal countrr: disp_agcgain: unsigned(5 downto 0) := to_unsigned(0, );
	
	signal output_lastvalue: std_logic_vector(26 downto 0) := (others => '0');
	
	signal level_output: std_logic_vector(2 downto 0):= std_logic_vector(to_unsigned(0, 3));
	signal level: unsigned(2 downto 0) := to_unsigned(0, 3);
	
	signal level_measurement: std_logic_vector(6 downto 0):= std_logic_vector(to_unsigned(0, 7));
	
	signal measurement_disp: std_logic := '0';
	
	signal dcf: std_logic;
	signal value0: unsigned(5 downto 0);
	signal value1: unsigned(5 downto 0);
   signal value2: unsigned(5 downto 0);
	signal value3: unsigned(5 downto 0); 
	
begin
	
	-- debouncers
	deb_switch0: entity work.debouncer port map(clk => clk, input => test_switches_dd(0), output => test_switches_deb(0));
	deb_switch1: entity work.debouncer port map(clk => clk, input => test_switches_dd(1), output => test_switches_deb(1));
	deb_switch2: entity work.debouncer port map(clk => clk, input => test_switches_dd(2), output => test_switches_deb(2));
	deb_switch3: entity work.debouncer port map(clk => clk, input => test_switches_dd(3), output => test_switches_deb(3));
	deb_button0: entity work.debouncer port map(clk => clk, input => test_buttons_dd(0), output => test_buttons_deb(0));
	deb_button1: entity work.debouncer port map(clk => clk, input => test_buttons_dd(1), output => test_buttons_deb(1));
	deb_button2: entity work.debouncer port map(clk => clk, input => test_buttons_dd(2), output => test_buttons_deb(2));
	deb_button3: entity work.debouncer port map(clk => clk, input => test_buttons_dd(3), output => test_buttons_deb(3));
	
	-- UART receiver (dummy)
	uart_rts_bar <= '0';
	
	-- UART transmitter
	uart_transmitter0: entity work.uart_transmitter port map(
		clk => clk,
		uart_tx => uart_tx,
		uart_cts_bar => uart_cts_bar,
		data => uart_transmitter_data,
		push => uart_transmitter_push
	);
	
	-- display driver
	display_driver0: entity work.display_driver port map(
		clk => clk,
		value0 => display_value0,
		value1 => display_value1,
		value2 => display_value2,
		value3 => display_value3,
		display_digits => display_digits,
		display_segments => display_segments
	);
	
	--envenlope detector
	evenlope_detector5: entity work.evenlope_detector port map( clk => clk, input => analog_comp_dd(5), output => evenlope(4) );
	evenlope_detector4: entity work.evenlope_detector port map( clk => clk, input => analog_comp_dd(4), output => evenlope(3) );
	evenlope_detector3: entity work.evenlope_detector port map( clk => clk, input => analog_comp_dd(3), output => evenlope(2) );
	evenlope_detector2: entity work.evenlope_detector port map( clk => clk, input => analog_comp_dd(2), output => evenlope(1) );
	evenlope_detector1: entity work.evenlope_detector port map( clk => clk, input => analog_comp_dd(1), output => evenlope(0) );
	
	--demodulator
	demodulator1: entity work.demodulator port map(
		clk => clk,
		input => evenlope,
		output => output_demodulator,
		output_treshold => treshold,
--		output_clock => clock,
		agc_lock => agc_lock,
	   agc_saturation => agc_saturation,
	   agc_value => agc_gain,
		output_lastvalue => output_lastvalue
	);
	
	--clock recovery
	clock_recovery: entity work.clock_recovery port map(
		clk => clk,
		evenlope_input => evenlope,
		clock_output => clock
	);
	
	--gain control
	gain_control: entity work.gain_control port map(
		clk => clk,
		evenlope_in => evenlope,
		treshold_input => treshold,
		gain_output => agc_gain,
		saturation => agc_saturation,
		lock => agc_lock,
		clock => clock,
		signal_level_output => level_output
	);
	
	--level_measurement
	level_measurement1: entity work.level_measurement port map(
		clk => clk,
		level_output => level_measurement,
		evenlope_in => evenlope
	);
	
		--decoder
	decoder_indicator: entity work.decoder_indicator port map(
	   clk => clk,
		dcf => output_demodulator,
		symbol0 => value0,
		symbol1 => value1,
		symbol2 => value2,
		symbol3 => value3
	);
	
	
	process(clk)
		variable test_value: unsigned(15 downto 0) := to_unsigned(12345, 16);
		variable disp_tencount: unsigned(5 downto 0) := to_unsigned(0, 6);
		variable disp_agcgain: unsigned(6 downto 0) := to_unsigned(0, 7);
		variable disp_compute: unsigned(6 downto 0) := to_unsigned(0, 7);
		variable disp_result: unsigned(6 downto 0) := to_unsigned(0, 7);
		
		 
		variable output: std_logic_vector(0 downto 0) := (others => '0');
		
	begin
		if clk'event and clk = '1' then
			
			-- delay elements to avoid metastability
			test_switches_d <= test_switches;
			test_switches_dd <= test_switches_d;
			test_buttons_d <= test_buttons;
			test_buttons_dd <= test_buttons_d;
			analog_comp_d <= analog_comp;
			analog_comp_dd <= analog_comp_d;
			
			-- example: read switches and buttons, write LEDs 
			test_leds(7 downto 4) <= test_switches_deb;
			test_leds(3 downto 0) <= test_buttons_deb;
			
			-- example: read analog comparator values, write analog gain
			--if analog_comp_dd = "010101" then
			--	analog_gain <= "111100";
			--else
			--	analog_gain <= "000011";
			--end if;
			
			if test_buttons_deb(0) /= manual_gain_bt0prev then
				manual_gain_bt0prev <= test_buttons_deb(0);
				if test_buttons_deb(0) = '1' and unsigned(agc_gain_manual(5 downto 0)) > 0 then
					agc_gain_manual <= std_logic_vector(unsigned(agc_gain_manual(5 downto 0)) - to_unsigned(1, 6));
				end if;
			end if;
			
			if test_buttons_deb(1) /= manual_gain_bt1prev then
				manual_gain_bt1prev <= test_buttons_deb(1);
				if test_buttons_deb(1) = '1' and unsigned(agc_gain_manual(5 downto 0)) < 63 then
					--agc_gain_manual <= agc_gain_manual + 1;
					agc_gain_manual <= std_logic_vector(unsigned(agc_gain_manual(5 downto 0)) + to_unsigned(1, 6));
				end if;
			end if;
			
			
			if test_switches_deb(0 downto 0) = "1" then
				measurement_disp <= '1';
			else 
				measurement_disp <= '0';
			end if;
			
			if test_buttons_deb(3) = '1' and manual_gain_button_state = '0' then
				manual_gain_button_state <= '1';
				if manual_gain = '0' then
					manual_gain <= '1';
					agc_gain_manual <= agc_gain;
				else 
					manual_gain <= '0';
				end if;
			else 
				manual_gain_button_state <= '0';
			end if;
			
			if manual_gain = '0' then
				test_leds(3) <= '0';
				analog_gain <= agc_gain;
				if measurement_disp = '0' then
					disp_agcgain := resize(unsigned(agc_gain(5 downto 0)),7);
				else
					disp_agcgain := unsigned(level_measurement);
				end if;
			else
				test_leds(3) <= '1';
				analog_gain <= agc_gain_manual;
				if measurement_disp = '0' then
					disp_agcgain := resize(unsigned(agc_gain_manual(5 downto 0)),7);
				else
					disp_agcgain := unsigned(level_measurement);
				end if;
			end if;
			
			if output_demodulator = '0' then
				output:= "0";
			else
				output:= "1";
			end if;
			
			-- example: write UART
			if counter1 >= 0 and counter1 <= 28 then
				case to_integer(counter1) is
					when 0 => uart_transmitter_data <= hex_table(to_integer(counter2(3 downto 0)));
					when 1 => uart_transmitter_data <= X"20";
					when 2 => uart_transmitter_data <= hex_table(to_integer(unsigned(test_switches_deb)));
					when 3 => uart_transmitter_data <= hex_table(to_integer(unsigned(test_buttons_deb)));
					when 4 => uart_transmitter_data <= X"20";
					when 5 => uart_transmitter_data <= hex_table(to_integer(disp_agcgain(6 downto 4)));
					when 6 => uart_transmitter_data <= hex_table(to_integer(disp_agcgain(3 downto 0)));
					when 7 => uart_transmitter_data <= X"20";
					--when 5 => uart_transmitter_data <= hex_table(to_integer(test_value(15 downto 12)));
					--when 6 => uart_transmitter_data <= hex_table(to_integer(test_value(11 downto 8)));
					--when 7 => uart_transmitter_data <= hex_table(to_integer(test_value(7 downto 4)));
					--when 8 => uart_transmitter_data <= hex_table(to_integer(test_value(3 downto 0)));
					--when 8 => uart_transmitter_data <= X"20";
					when 8 => uart_transmitter_data <= hex_table(to_integer(unsigned(treshold(26 downto 24))));
					when 9 => uart_transmitter_data <= hex_table(to_integer(unsigned(treshold(23 downto 20))));
					when 10 => uart_transmitter_data <= hex_table(to_integer(unsigned(treshold(19 downto 16))));
					when 11 => uart_transmitter_data <= hex_table(to_integer(unsigned(treshold(15 downto 12))));
					when 12 => uart_transmitter_data <= hex_table(to_integer(unsigned(treshold(11 downto 8))));
					when 13 => uart_transmitter_data <= hex_table(to_integer(unsigned(treshold(7 downto 4))));
					when 14 => uart_transmitter_data <= hex_table(to_integer(unsigned(treshold(3 downto 0))));
					when 15 => uart_transmitter_data <= X"20";
					when 16 => uart_transmitter_data <= hex_table(to_integer(unsigned(output_lastvalue(26 downto 24))));
					when 17 => uart_transmitter_data <= hex_table(to_integer(unsigned(output_lastvalue(23 downto 20))));
					when 18 => uart_transmitter_data <= hex_table(to_integer(unsigned(output_lastvalue(19 downto 16))));
					when 19 => uart_transmitter_data <= hex_table(to_integer(unsigned(output_lastvalue(15 downto 12))));
					when 20 => uart_transmitter_data <= hex_table(to_integer(unsigned(output_lastvalue(11 downto 8))));
					when 21 => uart_transmitter_data <= hex_table(to_integer(unsigned(output_lastvalue(7 downto 4))));
					when 22 => uart_transmitter_data <= hex_table(to_integer(unsigned(output_lastvalue(3 downto 0))));
					when 23 => uart_transmitter_data <= X"20";
					when 24 => uart_transmitter_data <= hex_table(to_integer(unsigned(level_measurement(6 downto 4))));
					when 25 => uart_transmitter_data <= hex_table(to_integer(unsigned(level_measurement(3 downto 0))));
					when 26 => uart_transmitter_data <= X"20";
					when 27 => uart_transmitter_data <= hex_table(to_integer(unsigned(output(0 downto 0))));
					when others => uart_transmitter_data <= X"0a";
				end case;
				uart_transmitter_push <= '1';
			else
				uart_transmitter_data <= "00000000";
				uart_transmitter_push <= '0';
			end if;
			
			-- example: write display
			if counter1 = to_unsigned(1999999, 21) then
				counter1 <= to_unsigned(0, 21);
				counter2 <= counter2 + 1;
			else
				counter1 <= counter1 + 1;
			end if;
			--display_value0 <= to_unsigned(13, 6); -- 13 = D
			
			display_value1 <= to_unsigned(0, 6) + unsigned(level_output(2 downto 0));
			
			if output_demodulator = '0' then 
				--display_value1 <= to_unsigned(0, 6); 
				display_value0 <= to_unsigned(21, 6); -- 21 = L
			else 
				--display_value1 <= to_unsigned(1, 6); 
				display_value0 <= to_unsigned(17, 6); -- 17 = H
			end if;
			
			--display_value2 <= to_unsigned(0, 6) + unsigned(treshold(26 downto 22));
			--display_value3 <= to_unsigned(0, 6) + unsigned(treshold(21 downto 17));
			--display_value2 <= to_unsigned(0, 6) + unsigned(agc_gain(5 downto 0)) / 10;
			
			--disp_agcgain := unsigned(gain_placeholder(5 downto 0));
			
			if counter1 = 0 then
				disp_tencount := to_unsigned(0, 7);
				disp_compute := disp_agcgain;
			else
				if disp_compute >= 10 then
					disp_compute := disp_compute - 10;
					disp_tencount := disp_tencount + 1;
				else 
					disp_result := disp_compute;
				end if;
			end if;
			
			--display_value3 <= to_unsigned(0, 6) + unsigned(agc_gain(5 downto 0)) mod 10; 
			--display_value2 <= to_unsigned(0, 6) + unsigned(agc_gain(5 downto 4));
			
			display_value2 <= to_unsigned(0, 6) + disp_tencount;
			display_value3 <= to_unsigned(0, 6) + disp_result;
			
			
			display_colon <= clock;
			
			--if clock /= clock_prev then
			--	clock_prev <= clock;
			--	if unsigned(gain_placeholder(5 downto 0)) < 63 then
			--		gain_placeholder <= std_logic_vector(unsigned(gain_placeholder(5 downto 0)) + to_unsigned(1, 6));
			--	else
			--		gain_placeholder <= std_logic_vector(to_unsigned(0, 6));
			--	end if;
			--end if;
		end if;
	end process;
	 
end bhv;
