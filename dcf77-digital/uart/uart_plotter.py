from pylab import *
import os
import scipy.io
import serial
import time

# data configuration
samplerate = 50
samples = samplerate * 5
names = ["Counter", "Switches/buttons", "Test value", "Counter3"]

# interface configuration
baudrate = 1000000
timeout = 1.0

def xlims(a, b):
	f = gcf()
	for ax in f.get_axes():
		ax.set_xlim(a, b)
	f.canvas.draw()

def ylims(a, b):
	f = gcf()
	for ax in f.get_axes():
		ax.set_ylim(a, b)
	f.canvas.draw()

# find serial port
serialport = None
for i in range(10):
	test = "/dev/ttyUSB%d" % (i)
	if os.path.exists(test):
		serialport = test
		break
if serialport is None:
	raise Exception("Can't find serial port! Check the cable.")

# open serial port
ser = serial.Serial(port=serialport, baudrate=baudrate, bytesize=8, parity=serial.PARITY_NONE, stopbits=1, timeout=timeout, rtscts=True)
print("Using port: " + ser.name)
print("Starting time: " + time.strftime("%H:%M:%S"))

data = None
rows = 0

# start reading
try:
	
	# clear the buffer
	ser.timeout = 0.1
	starttime = time.time()
	while time.time() < starttime + 0.1:
		ser.read(1)
	ser.timeout = timeout
	
	# ignore the first line (may be incomplete)
	ser.readline()
	
	# read lines
	while rows < samples:
		line = ser.readline().decode("latin1")
		if line.endswith("\n"):
			print("\rReading %d/%d ..." % (rows + 1, samples), end="")
			#print(">> " + line[:-1])
			words = line[:-1].split(" ")
			if data is None:
				data = zeros((samples, len(words)), dtype=int64)
			elif data.shape[1] != len(words):
				print(" parse error!")
				print("Can't parse this line: " + repr(line))
				print("Expected %d words, got %d instead." % (data.shape[1], len(words)))
				break
			try:
				data[rows] = [int(w, 16) for w in words]
			except ValueError as e:
				print(" parse error!")
				print("Can't parse this line: " + repr(line))
				print(str(e.__class__) + ": " + str(e))
				break
			rows += 1
		else:
			print(" timeout!")
			break
	
except KeyboardInterrupt:
	print(" interrupted!")
except Exception as e:
	print(" exception!")
	print(str(e.__class__) + ": " + str(e))

# trim data if necessary
if rows == samples:
	print(" done!")
elif data is not None:
	data = data[:rows]

# close serial port
ser.close()

if data is not None:
	
	# save data
	scipy.io.savemat("uart_data.mat", {"data": data})
	
	# plot data
	close("all")
	figure("UART plotter", figsize=(20, 12))
	for i in range(data.shape[1]):
		if i == 0:
			ax = subplot(data.shape[1], 1, i + 1)
		else:
			subplot(data.shape[1], 1, i + 1, sharex=ax)
		plot(arange(rows) / samplerate, data[:, i])
		grid()
		if i < len(names):
			ylabel(names[i])
	tight_layout()
	show()

