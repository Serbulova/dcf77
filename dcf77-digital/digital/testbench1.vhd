library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity testbench1 is
end testbench1;

architecture bhv of testbench1 is
	
	signal clk: std_logic := '0';
	signal analog_comp: std_logic_vector(5 downto 0) := (others => '0');
	
	constant clk_period: time := 50 ns;
	
	-- csv reader definition
	component csvreader is
		generic(
			clk_period: integer;
			csvfile: string
		);
		port(
			data: out std_logic
		);
	end component;
	
begin
	
	-- comparator input
	csvreader_comp0: entity work.csvreader generic map(clk_period => 50, csvfile => "wave3_comp0.csv") port map(data => analog_comp(0));
	csvreader_comp1: entity work.csvreader generic map(clk_period => 50, csvfile => "wave3_comp1.csv") port map(data => analog_comp(1));
	csvreader_comp2: entity work.csvreader generic map(clk_period => 50, csvfile => "wave3_comp2.csv") port map(data => analog_comp(2));
	csvreader_comp3: entity work.csvreader generic map(clk_period => 50, csvfile => "wave3_comp3.csv") port map(data => analog_comp(3));
	csvreader_comp4: entity work.csvreader generic map(clk_period => 50, csvfile => "wave3_comp4.csv") port map(data => analog_comp(4));
	csvreader_comp5: entity work.csvreader generic map(clk_period => 50, csvfile => "wave3_comp5.csv") port map(data => analog_comp(5));
	
	-- clock process
	process
	begin
		clk <= '0';
		wait for clk_period;
		while true loop
			clk <= '1';
			wait for clk_period/2;
			clk <= '0';
			wait for clk_period/2;
		end loop;
	end process;
	
end;
