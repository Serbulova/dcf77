`timescale 1ns / 1ps

module csvreader(
	output data
);

parameter clk_period = 0;
parameter csvfile = "";

integer f;
integer cur_time;
integer cur_value;
initial begin
	f = $fopen(csvfile, "r");
	if(f == 0) begin
		$display("Error: Can't open CSV file!");
		$finish;
	end
	cur_time = 0;
end

integer vec_time;
integer vec_value;
always begin
	if($feof(f)) begin
		$finish;
	end
	$fscanf(f, "%d,%d\n", vec_time, vec_value);
	if(vec_time > cur_time) begin
		#(clk_period * (vec_time - cur_time));
	end
	cur_time = vec_time;
	cur_value = vec_value;
end

assign data = cur_value;

endmodule
